def cleaned_form_to_lists(form):
    name = form['name'].upper()
    address = form['address']
    city = form['city']
    zip = form['zip']
    county = form['county']
    state = form['state']+"|"+abbreviate_state(form['state'])
    type = form['type']
    
    er = ""
    if(form['erYes']):
        er+="Yes|"
    if(form['erNo']):
        er+="No|"
    if(er == ""):
        er = "Yes|No|"
    er = er[:-1]
    
    ownership = ""
    if(form['government']):
        ownership+="Government.*|"
    if(form['nonprofit']):
        ownership+="Voluntary non-profit.*|"
    if(form['physician']):
        ownership+="Physician|"
    if(form['proprietary']):
        ownership+="Proprietary|"
    if(form['tribal']):
        ownership+="Tribal|"
    if(ownership == ""):
        ownership = "Government|Nonprofit|Physician|Proprietary|Tribal|"
    ownership = ownership[:-1]
    
    rating = ""
    if(form['rate1']):
        rating+="1|"
    if(form['rate2']):
        rating+="2|"
    if(form['rate3']):
        rating+="3|"
    if(form['rate4']):
        rating+="4|"
    if(form['rate5']):
        rating+="5|"
    if(form['ratena']):
        rating+="Not Available|"
    if(rating == ""):
        rating = "1|2|3|4|5|Not Available|"
    rating = rating[:-1]
    
    mortality = ""
    if(form['mortalityAbove']):
        mortality+="Above the national average|"
    if(form['mortalityBelow']):
        mortality+="Below the national average|"
    if(form['mortalityEqual']):
        mortality+="Same as the national average|"
    if(form['mortalityNa']):
        mortality+="Not Available|"
    mortality = mortality[:-1]
    
    safety = ""
    if(form['safetyAbove']):
        safety+="Above the national average|"
    if(form['safetyBelow']):
        safety+="Below the national average|"
    if(form['safetyEqual']):
        safety+="Same as the national average|"
    if(form['safetyNa']):
        safety+="Not Available|"
    safety = safety[:-1]
    
    readmission = ""
    if(form['readmissionAbove']):
        readmission+="Above the national average|"
    if(form['readmissionBelow']):
        readmission+="Below the national average|"
    if(form['readmissionEqual']):
        readmission+="Same as the national average|"
    if(form['readmissionNa']):
        readmission+="Not Available|"
    readmission = readmission[:-1]
    
    patientExperience = ""
    if(form['patientExperienceAbove']):
        patientExperience+="Above the national average|"
    if(form['patientExperienceBelow']):
        patientExperience+="Below the national average|"
    if(form['patientExperienceEqual']):
        patientExperience+="Same as the national average|"
    if(form['patientExperienceNa']):
        patientExperience+="Not Available|"
    patientExperience = patientExperience[:-1]
    
    effectiveness = ""
    if(form['effectivenessAbove']):
        effectiveness+="Above the national average|"
    if(form['effectivenessBelow']):
        effectiveness+="Below the national average|"
    if(form['effectivenessEqual']):
        effectiveness+="Same as the national average|"
    if(form['effectivenessNa']):
        effectiveness+="Not Available|"
    effectiveness = effectiveness[:-1]
    
    timeliness = ""
    if(form['timelinessAbove']):
        timeliness+="Above the national average|"
    if(form['timelinessBelow']):
        timeliness+="Below the national average|"
    if(form['timelinessEqual']):
        timeliness+="Same as the national average|"
    if(form['timelinessNa']):
        timeliness+="Not Available|"
    timeliness = timeliness[:-1]
    
    medicalImaging = ""
    if(form['medicalImagingAbove']):
        medicalImaging+="Above the national average|"
    if(form['medicalImagingBelow']):
        medicalImaging+="Below the national average|"
    if(form['medicalImagingEqual']):
        medicalImaging+="Same as the national average|"
    if(form['medicalImagingNa']):
        medicalImaging+="Not Available|"
    medicalImaging = medicalImaging[:-1]
    
    return name,address,city,zip,county,state,type,er,ownership,rating,mortality,safety,readmission, patientExperience,effectiveness,timeliness,medicalImaging

def abbreviation_to_name(abbrev):
    if(abbrev=='AL'):
        return 'Alabama'
    if(abbrev=='AK'):
        return 'Alaska'
    if(abbrev=='AZ'):
        return 'Arizona'
    if(abbrev=='AR'):
        return 'Arkansas'
    if(abbrev=='CA'):
        return 'California'
    if(abbrev=='CO'):
        return 'Colorado'
    if(abbrev=='CT'):
        return 'Connecticut'
    if(abbrev=='DE'):
        return 'Delaware'
    if(abbrev=='DC'):
        return 'District of Columbia'
    if(abbrev=='FL'):
        return 'Florida'
    if(abbrev=='GA'):
        return 'Georgia'
    if(abbrev=='HI'):
        return 'Hawaii'
    if(abbrev=='ID'):
        return 'Idaho'
    if(abbrev=='IL'):
        return 'Illinois'
    if(abbrev=='IN'):
        return 'Indiana'
    if(abbrev=='IA'):
        return 'Iowa'
    if(abbrev=='KS'):
        return 'Kansas'
    if(abbrev=='KY'):
        return 'Kentucky'
    if(abbrev=='LA'):
        return 'Louisiana'
    if(abbrev=='ME'):
        return 'Maine'
    if(abbrev=='MD'):
        return 'Maryland'
    if(abbrev=='MA'):
        return 'Massachusetts'
    if(abbrev=='MI'):
        return 'Michigan'
    if(abbrev=='MN'):
        return 'Minnesota'
    if(abbrev=='MS'):
        return 'Mississippi'
    if(abbrev=='MO'):
        return 'Missouri'
    if(abbrev=='MT'):
        return 'Montana'
    if(abbrev=='NE'):
        return 'Nebraska'
    if(abbrev=='NV'):
        return 'Nevada'
    if(abbrev=='NH'):
        return 'New Hampshire'
    if(abbrev=='NJ'):
        return 'New Jersey'
    if(abbrev=='NM'):
        return 'New Mexico'
    if(abbrev=='NY'):
        return 'New York'
    if(abbrev=='NC'):
        return 'North Carolina'
    if(abbrev=='ND'):
        return 'North Dakota'
    if(abbrev=='OH'):
        return 'Ohio'
    if(abbrev=='OK'):
        return 'Oklahoma'
    if(abbrev=='OR'):
        return 'Oregon'
    if(abbrev=='PA'):
        return 'Pennsylvania'
    if(abbrev=='RI'):
        return 'Rhode Island'
    if(abbrev=='SC'):
        return 'South Carolina'
    if(abbrev=='SD'):
        return 'South Dakota'
    if(abbrev=='TN'):
        return 'Tennessee'
    if(abbrev=='TX'):
        return 'Texas'
    if(abbrev=='UT'):
        return 'Utah'
    if(abbrev=='VT'):
        return 'Vermont'
    if(abbrev=='VA'):
        return 'Virginia'
    if(abbrev=='WA'):
        return 'Washington'
    if(abbrev=='WV'):
        return 'West Virginia'
    if(abbrev=='WI'):
        return 'Wisconsin'
    if(abbrev=='WY'):
        return 'Wyoming'
    if(abbrev=='AS'):
        return 'American Samoa'
    if(abbrev=='GU'):
        return 'Guam'
    if(abbrev=='MP'):
        return 'Northern Mariana Islands'
    if(abbrev=='PR'):
        return 'Puerto Rico'
    if(abbrev=='VI'):
        return 'U.S. Virgin Islands'
    if(abbrev=='UM'):
        return 'U.S. Minor Outlying Islands'
    if(abbrev=='MH'):
        return 'Marshall Islands'
    if(abbrev=='FM'):
        return 'Micronesia'
    if(abbrev=='PW'):
        return 'Palau'
    return abbrev
    
def abbreviate_state(abbrev):
    if(abbrev=='Alabama'):
        return 'AL'
    if(abbrev=='Alaska'):
        return 'AK'
    if(abbrev=='Arizona'):
        return 'AZ'
    if(abbrev=='Arkansas'):
        return 'AR'
    if(abbrev=='California'):
        return 'CA'
    if(abbrev=='Colorado'):
        return 'CO'
    if(abbrev=='Connecticut'):
        return 'CT'
    if(abbrev=='Delaware'):
        return 'DE'
    if(abbrev=='District of Columbia'):
        return 'DC'
    if(abbrev=='Florida'):
        return 'FL'
    if(abbrev=='Georgia'):
        return 'GA'
    if(abbrev=='Hawaii'):
        return 'HI'
    if(abbrev=='Idaho'):
        return 'ID'
    if(abbrev=='Illinois'):
        return 'IL'
    if(abbrev=='Indiana'):
        return 'IN'
    if(abbrev=='Iowa'):
        return 'IA'
    if(abbrev=='Kansas'):
        return 'KS'
    if(abbrev=='Kentucky'):
        return 'KY'
    if(abbrev=='Louisiana'):
        return 'LA'
    if(abbrev=='Maine'):
        return 'ME'
    if(abbrev=='Maryland'):
        return 'MD'
    if(abbrev=='Massachusetts'):
        return 'MA'
    if(abbrev=='Michigan'):
        return 'MI'
    if(abbrev=='Minnesota'):
        return 'MN'
    if(abbrev=='Mississippi'):
        return 'MS'
    if(abbrev=='Missouri'):
        return 'MO'
    if(abbrev=='Montana'):
        return 'MT'
    if(abbrev=='Nebraska'):
        return 'NE'
    if(abbrev=='Nevada'):
        return 'NV'
    if(abbrev=='New Hampshire'):
        return 'NH'
    if(abbrev=='New Jersey'):
        return 'NJ'
    if(abbrev=='New Mexico'):
        return 'NM'
    if(abbrev=='New York'):
        return 'NY'
    if(abbrev=='North Carolina'):
        return 'NC'
    if(abbrev=='North Dakota'):
        return 'ND'
    if(abbrev=='Ohio'):
        return 'OH'
    if(abbrev=='Oklahoma'):
        return 'OK'
    if(abbrev=='Oregon'):
        return 'OR'
    if(abbrev=='Pennsylvania'):
        return 'PA'
    if(abbrev=='Rhode Island'):
        return 'RI'
    if(abbrev=='South Carolina'):
        return 'SC'
    if(abbrev=='South Dakota'):
        return 'SD'
    if(abbrev=='Tennessee'):
        return 'TN'
    if(abbrev=='Texas'):
        return 'TX'
    if(abbrev=='Utah'):
        return 'UT'
    if(abbrev=='Vermont'):
        return 'VT'
    if(abbrev=='Virginia'):
        return 'VA'
    if(abbrev=='Washington'):
        return 'WA'
    if(abbrev=='West Virginia'):
        return 'WV'
    if(abbrev=='Wisconsin'):
        return 'WI'
    if(abbrev=='Wyoming'):
        return 'WY'
    if(abbrev=='American Samoa'):
        return 'AS'
    if(abbrev=='Guam'):
        return 'GU'
    if(abbrev=='Northern Mariana Islands'):
        return 'MP'
    if(abbrev=='Puerto Rico'):
        return 'PR'
    if(abbrev=='U.S. Virgin Islands'):
        return 'VI'
    if(abbrev=='U.S. Minor Outlying Islands'):
        return 'UM'
    if(abbrev=='Marshall Islands'):
        return 'MH'
    if(abbrev=='Micronesia'):
        return 'FM'
    if(abbrev=='Palau'):
        return 'PW'
    return abbrev
