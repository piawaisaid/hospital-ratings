from django import forms

class QueryForm(forms.Form):
    #Hospital name
    name = forms.CharField(max_length=50, required=False)
    
    #Hospital location
    address = forms.CharField(max_length=80, required=False)
    city = forms.CharField(max_length=15, required=False)
    zip = forms.CharField(max_length=5, required=False)
    county = forms.CharField(max_length=15, required=False)
    state = forms.CharField(max_length=15, required=False)
    
    #Hospital type
    type = forms.CharField(max_length=50, required=False)

    #Emergency Service
    erYes = forms.BooleanField(required=False, initial=True)
    erNo = forms.BooleanField(required=False, initial=True)
    
    #Ownership
    government = forms.BooleanField(required=False, initial=True)
    nonprofit = forms.BooleanField(required=False, initial=True)
    physician = forms.BooleanField(required=False, initial=True)
    proprietary = forms.BooleanField(required=False, initial=True)
    tribal = forms.BooleanField(required=False, initial=True)
    
    #OveralRatings
    rate1 = forms.BooleanField(required=False, initial=True)
    rate2 = forms.BooleanField(required=False, initial=True)
    rate3 = forms.BooleanField(required=False, initial=True)
    rate4 = forms.BooleanField(required=False, initial=True)
    rate5 = forms.BooleanField(required=False, initial=True)
    ratena = forms.BooleanField(required=False, initial=True)
    
    #National Comparisons
    mortalityBelow = forms.BooleanField(required=False, initial=True)
    mortalityAbove = forms.BooleanField(required=False, initial=True)
    mortalityEqual = forms.BooleanField(required=False, initial=True)
    mortalityNa = forms.BooleanField(required=False, initial=True)
    
    safetyBelow = forms.BooleanField(required=False, initial=True)
    safetyAbove = forms.BooleanField(required=False, initial=True)
    safetyEqual = forms.BooleanField(required=False, initial=True)
    safetyNa = forms.BooleanField(required=False, initial=True)
    
    readmissionBelow = forms.BooleanField(required=False, initial=True)
    readmissionAbove = forms.BooleanField(required=False, initial=True)
    readmissionEqual = forms.BooleanField(required=False, initial=True)
    readmissionNa = forms.BooleanField(required=False, initial=True)
    
    patientExperienceBelow = forms.BooleanField(required=False, initial=True)
    patientExperienceAbove = forms.BooleanField(required=False, initial=True)
    patientExperienceEqual = forms.BooleanField(required=False, initial=True)
    patientExperienceNa = forms.BooleanField(required=False, initial=True)
    
    effectivenessBelow = forms.BooleanField(required=False, initial=True)
    effectivenessAbove = forms.BooleanField(required=False, initial=True)
    effectivenessEqual = forms.BooleanField(required=False, initial=True)
    effectivenessNa = forms.BooleanField(required=False, initial=True)
    
    timelinessBelow = forms.BooleanField(required=False, initial=True)
    timelinessAbove = forms.BooleanField(required=False, initial=True)
    timelinessEqual = forms.BooleanField(required=False, initial=True)
    timelinessNa = forms.BooleanField(required=False, initial=True)
    
    medicalImagingBelow = forms.BooleanField(required=False, initial=True)
    medicalImagingAbove = forms.BooleanField(required=False, initial=True)
    medicalImagingEqual = forms.BooleanField(required=False, initial=True)
    medicalImagingNa = forms.BooleanField(required=False, initial=True)

    SORT_BY_CHOICES = [
        ('?name', 'Name'),
        ('?overallRating', 'Rating')
    ]
    sortBy = forms.CharField(label='Sort By', widget=forms.Select(choices=SORT_BY_CHOICES))

    SORT_OPTIONS=[
        ('ASC','Ascending'),
        ('DESC','Descending')
    ]

    sortOptions = forms.ChoiceField(label='Sort Options', initial=('ASC','Ascending'), choices=SORT_OPTIONS, widget=forms.RadioSelect)