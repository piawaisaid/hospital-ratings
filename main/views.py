from django.shortcuts import render
from .forms import QueryForm
from rdflib import Graph
from SPARQLWrapper import SPARQLWrapper, JSON, N3
from pprint import pprint
import ssl
from pymantic import sparql as sparql
from .util import *

def home(request):
    return render(request, 'main/home.html')

def search(request):
    if request.method == 'POST':
        form = QueryForm(request.POST)
        if form.is_valid():
            print(form.cleaned_data)
        name,address,city,zip,county,\
        state,type,er,ownership,rating,\
        mortality,safety,readmission, \
        patientExperience,effectiveness,\
        timeliness,medicalImaging = cleaned_form_to_lists(form.cleaned_data)
        query = '''
            select *
            where {?hospital <ex:name> ?name.
                   ?hospital <ex:overallRating> ?overallRating.
                   ?hospital <ex:hospitalType> ?hospitalType.
                   ?hospital <ex:phoneNumber> ?phoneNumber.
                   ?hospital <ex:address> ?address.
                   ?hospital <ex:city> ?city.
                   ?hospital <ex:postalCode> ?postalCode.
                   ?hospital <ex:county> ?county.
                   ?hospital <ex:emergencyService> ?emergencyService.
                   ?hospital <ex:hospitalType> ?hospitalType.
                   ?hospital <ex:mortalityComparison> ?mortalityComparison.
                   ?hospital <ex:ownership> ?ownership.
                   ?hospital <ex:state> ?state.
                   ?hospital <ex:effectiveness> ?effectiveness.
                   ?hospital <ex:medicalImaging> ?medicalImaging.
                   ?hospital <ex:readmission> ?readmission.
                   ?hospital <ex:safetyCare> ?safetyCare.
                   ?hospital <ex:timeliness> ?timeliness.
                  FILTER ( 
                  (regex(?name,".*'''+ name +'''.*","i")) 
                  && (regex(?hospitalType,".*'''+ type +'''.*","i")) 
                  && (regex(?address,".*'''+ address +'''.*","i")) 
                  && (regex(?city,".*'''+ city +'''.*","i"))
                  && (regex(?state,".*'''+ state +'''.*","i"))
                  && (regex(?overallRating,"'''+ rating +'''","i"))
                  && (regex(?postalCode,".*'''+ zip +'''.*","i"))
                  && (regex(?county,".*'''+ county +'''.*","i"))
                  && (regex(?emergencyService,"'''+ er +'''","i"))
                  && (regex(?mortalityComparison,"'''+ mortality +'''","i"))
                  && (regex(?ownership,"'''+ ownership +'''","i"))
                  && (regex(?effectiveness,".*'''+ effectiveness +'''.*","i"))
                  && (regex(?medicalImaging,".*'''+ medicalImaging +'''.*","i"))
                  && (regex(?readmission,".*'''+ readmission +'''.*","i"))
                  && (regex(?safetyCare,".*'''+ safety +'''.*","i"))
                  && (regex(?timeliness,".*'''+ timeliness +'''.*","i"))
                  ).
            }
            ORDER BY ''' + form.cleaned_data['sortOptions'] + '''(''' + form.cleaned_data['sortBy'] + ''')''''''
        '''

        print(query)
        sparql = SPARQLWrapper('http://192.168.78.188:9999/blazegraph/sparql')
        sparql.setQuery(
        query
        )

        sparql.setReturnFormat(JSON)
        qres = sparql.query().convert()
    else:
        form = QueryForm()
        qres = {'head': {'vars': ['hospital', 'name', 'overallRating', 'hospitalType', 'address', 'emergencyService']}, 'results': {'bindings': []}}
    
    return render(request, 'search.html', {'qres' : qres, 'form':form})

def detail(request, id):
    sparql = SPARQLWrapper('http://192.168.78.188:9999/blazegraph/sparql')
    sparql = SPARQLWrapper('http://192.168.78.188:9999/blazegraph/sparql')
    query = '''
    select *
    where {?hospital <ex:name> ?name.
           ?hospital <ex:overallRating> ?overallRating.
           ?hospital <ex:hospitalType> ?hospitalType.
           ?hospital <ex:phoneNumber> ?phoneNumber.
           ?hospital <ex:address> ?address.
           ?hospital <ex:city> ?city.
           ?hospital <ex:postalCode> ?postalCode.
           ?hospital <ex:county> ?county.
           ?hospital <ex:emergencyService> ?emergencyService.
           ?hospital <ex:hospitalType> ?hospitalType.
           ?hospital <ex:mortalityComparison> ?mortalityComparison.
           ?hospital <ex:ownership> ?ownership.
           ?hospital <ex:state> ?state.
           ?hospital <ex:effectiveness> ?effectiveness.
           ?hospital <ex:medicalImaging> ?medicalImaging.
           ?hospital <ex:readmission> ?readmission.
           ?hospital <ex:safetyCare> ?safetyCare.
           ?hospital <ex:timeliness> ?timeliness.
          FILTER ( ?hospital = <'''+ id +'''> ).
    }
    '''
    sparql.setQuery(query)

    sparql.setReturnFormat(JSON)
    qres = sparql.query().convert()
    
    hospital = qres['results']['bindings'][0]
    
    ssl._create_default_https_context = ssl._create_unverified_context

    sparql = SPARQLWrapper('https://dbpedia.org/sparql')
    query = '''
    select *
    where {?county dbp:county ?countyName.
           ?county dbp:state ?stateName.
           ?county rdf:type dbo:Region.
           ?county dbo:abstract ?abstract.
          FILTER (
          regex(?countyName, "'''+hospital['county']['value']+'''.*","i")
          && regex(?stateName, "'''+abbreviation_to_name(hospital['state']['value'])+'''.*","i")
          && lang(?abstract) = 'en'
          ).
    }
    '''
    sparql.setQuery(query)

    sparql.setReturnFormat(JSON)
    countyDetail = sparql.query().convert()

    city = hospital['city']['value']
    sparql = SPARQLWrapper('https://dbpedia.org/sparql')
    query = '''
    select distinct ?city ?name
            where {?county dbp:city ?city .
           ?city dbp:name ?name.
           FILTER regex(?name, "'''+city+'''", "i").
            }
    LIMIT 1
    '''
    sparql.setQuery(query)

    sparql.setReturnFormat(JSON)
    countyURI = sparql.query().convert()

    name = hospital['name']['value']
    sparql = SPARQLWrapper('https://dbpedia.org/sparql')
    query = '''
    select distinct ?hospital ?name ?bedCount ?abstract
            where {?hospital rdf:type dbo:Hospital.
           ?hospital dbp:name ?name.
           ?hospital dbo:bedCount ?bedCount .
           ?hospital dbo:abstract ?abstract .
           FILTER regex(?name, "'''+name+'''", "i").
            }
    LIMIT 1
    '''
    sparql.setQuery(query)

    sparql.setReturnFormat(JSON)
    nameURI = sparql.query().convert()

    state = hospital['state']['value']
    sparql = SPARQLWrapper('https://dbpedia.org/sparql')
    stateName = abbreviation_to_name(state)
    print(stateName)
    query = '''
    select distinct ?state ?name
            where {?county dbo:state ?state .
           ?state dbp:name ?name.
           FILTER regex(?name, "'''+stateName+'''", "i").
            }
    LIMIT 1
    '''
    sparql.setQuery(query)

    sparql.setReturnFormat(JSON)
    stateURI = sparql.query().convert()



    return render(request, 'detail.html', {
        'qres' : qres,
        'countyDetail' : countyDetail,
        'countyURI' : countyURI,
        'nameURI' : nameURI,
        'stateURI' : stateURI
        })
